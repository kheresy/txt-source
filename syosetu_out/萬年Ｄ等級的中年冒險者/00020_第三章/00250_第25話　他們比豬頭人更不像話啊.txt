「噗唏⋯⋯！？」

右臂被我斬飛的奧克領主發出慘叫。

然後艾莉婭的火焰把它的巨體包圍。

「盧卡斯殿。」
「主君！」
「您沒事吧，盧卡斯大人！」

琉娜們跑了過來。

「哈⋯⋯？是叫盧卡斯⋯⋯？」

聽到腳邊附近有人這麼說，我轉過頭看了一眼。

恐怕是奧克領主來到面前，被揮下的大木棍時嚇到了吧，冒険者們還在嗶哩嗶哩地渾身發抖，目瞪口呆的。
如果柯露雪沒有用【暗影綁定】來拖延奧克領主的攻擊，而我並沒有砍下右臂的話，現在肯定已經變成肉渣了吧。

「那（傢伙）是盧卡斯⋯⋯？」

⋯⋯哎！？

「不好！」

我發現他們中有熟識的面孔，不由自主地說漏了嘴。

『呵～？這些傢伙，在哪裡見過呢？』

是雷克。
薩魯賈也在。

雖然現在涕淚交加慘不忍睹的臉，但卻曾經是我所屬的隊伍成員。

『是啊是啊，我想起來了。這些傢伙們，在戮殺豹手上差點被殺了的時候，是你救下了吧！但是那個小姑娘不在嗎？』

應該是因為那件事，和瑪麗鬧矛盾分開了。
有三個我不認識的成員，是我們離開之後才進來的吧！

「怎麼了，盧卡斯殿？」

琉娜覺得奇怪，歪著頭來問我。

所以現在不要用那個名字叫我⋯⋯⋯

不管怎麼說，在我拿到維納斯之後，就自稱「魯卡」裝小弟（作為弟弟身份）

好不容易被誤解為別人，如果可能的話，真不想被發現是本人啊。

「現，現在不是說這個的時候。得快點把奧克領主幹掉。」

連忙敷衍過去，朝著奧克領主擺起架式。

「真是太頑強了吶，這傢伙」
「嗚～，快要壓制不住啦～！」

雖然在艾莉婭的烈焰中炙烤，但為了逃脫柯露雪的困縛讓全身的肌肉突然膨脹起來。

「噗喔喔⋯⋯！！」

終於強行打破柯露雪的影縛，揮舞著剩下的左臂襲來。

「琉娜。」
「收到了。」

琉娜射出的箭與奧克領主的拳頭髮生激烈衝突。

箭矢雖然被破壊，但同時奧克領主的拳頭也被彈到身後。

「喔哦哦⋯⋯！」

我腳用力一跳，掄起維納斯就一招旋風斬裂，砍進奧克領主的頭上。

終於奧克領主在眼裡代表意志的高光消失，巨大身體往後面倒伏。

慢慢地化為灰燼。

「哦，喂，你⋯⋯難道，是那個盧卡斯嗎⋯⋯？」

呆然看著戰鬥的落幕後，雷克戰戰兢兢地地問道。

這下完全暴露了吶⋯⋯⋯

「就這樣！這位才是我們的主公，盧卡斯大人啊！」
「面對主君的慈悲救助，你們快表示感謝啊！」

羅和羅恩一唱一和地配合說著。
請不要這樣，特別是把我捧得高高在上⋯⋯⋯

「總，總而言之，先離開森林吧！奧克領主被打倒了，但有還是覺得不對勁。應該回去報告。」

特地說明很麻煩，更重要的是現在沒有那種餘裕了。
我強行改變了話題走向，朝著森林外面方向走出去。

「收到了，主君！」
「喂，你們這些傢伙。如果不想死就跟著⋯⋯」
「當，當然了⋯⋯！」
「拜託，請不要捨棄我們⋯⋯」

雷克他們也勉強站了起來，晃悠悠地在後面屁顛。


◇◇◇◇◇◇

「差不多該到時候了。」

卡爾茲的女領主伊莎貝拉，在自己率領的騎士團精銳部隊前頭自言自語。

為了分散奧克族群，以冒険者為主組成的戰鬥力突入森林已經過了一段時間。
恐怕森林的各處，現在已經開始交戰了吧。

「為了討伐奧克領主，我們也突入森林吧！」

伊莎貝拉如此宣言後，騎士們高聲吶喊。
就在那時候。

「太好了，出來了⋯⋯！」
「以為（這次）死定了呢，可惡咧！」

一群人從森林裡衝了出來，看來是冒険者。

一臉焦灼的神情，身上到處都受了傷。
已經撐不住要撤退了吧。

「真是的，他們比豬頭人更不像話啊！」

伊莎貝拉嘆了口氣。
但是反正也只是冒険者而已。

一個、兩個隊伍撤退還在預想之內。

「我可沒聽說過啊！」
「那些奧克太強了吧！真的只是奧克嗎！？」

但是，如果像這樣有第三個、第四個地，隊伍一個接一個從森林裡撤退出來的話，就不得不考慮，是有什麼異常事態發生了。

「伊莎貝拉大人！這個聲音到底是⋯⋯」
「聲音？」

矗嗯、矗嗯、嘭咚嘭咚嘭咚──

被屬下提醒，豎起耳朵聽著，聽到樹葉在風中摩擦的撒拉撒拉聲，夾雜著謎之聲音。
是有什麼東西從森林深處向這邊靠近了。
身高超過四米的豬怪，不知為何出現在她們陣前。

「什⋯⋯奧克領主⋯⋯？」

最重要的討伐對象自己出現送上門來。

「⋯⋯呵。沒想到，居然會從那邊來到我們陣前。還省去特意尋找的工夫呢⋯⋯」

伊莎貝拉無畏地笑著，馬上做好交戰的準備。

就在那時候。

噗嗚，的一聲。

比奧克領主還要大一圈的豬頭怪物，雖遲了點也從樹林裡出來了。

「『『⋯⋯啊！？』』」

面對難以置信的事態，騎士們嚇得喘不過氣、啞口無言。

「難，難道說⋯⋯那是⋯⋯」

想到了那個真面目，伊莎貝拉聲音顫抖起來。

那是被稱為奧克之王，比奧克領主更高一級的存在。
可以說是王中之王。
又或者說是皇帝。

「豬頭帝（奧克魔王）⋯⋯？」