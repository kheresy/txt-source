接連不斷來找我的諮詢者，因為我把公主和王子作為窗口，所以人數減少了很多，幾乎不會有人來找我了。
終於可以自由自在地家裏蹲了，於是在邊境伯領的木屋裡欣賞著動畫片。就在這個時候。

「兄長，我回來了！」

夏洛特精神飽滿地回來了。這孩子在上學院的時候，住在這個木屋裡。（雖然她在學生宿舍裡有一個房間）

「哦，夏爾，歡迎回來………………怎麼了？」

夏爾一放下書包就到處亂跑。制服要保持原狀，還要整理頭髮、檢查儀容。

「我現在要去見一個人，得注意不要失禮。」

嗯，是貴族的重要人物嗎？雖然我完全沒有參與，但作為邊境伯這一重要地位的女兒，夏爾雖然年紀小，卻經常在社交界之類的場合露面……是嗎？

「那麼，兄長，我走了！」

對幹勁十足地出門的妹妹，不安地抬起了頭。
打扮得漂漂亮亮（或許還不至於）要會面的人，究竟是怎樣的人呢？

「難道是男的？」

哈哈哈，夏爾不會吧，哈哈哈……。

「不是不可能！？」

一想到這個我就非常不安。不，但是，嗯，是啊。
作為哥哥，我有義務確認對方是誰。因為我是哥哥。因為是哥哥。哥哥就是這樣的吧？啊？

我可以派出監視用的結界來在這裡確認。我是個家裏蹲Love，雖然很想這麼做，但總覺得會窺探妹妹的私生活，所以會猶豫也是事實。

於是，用光學迷彩結界消去身影，跟在後面。這樣的話可不是偷窺。堂堂正正的看著呢！（不考慮隱藏了）

――於是。
夏爾返回學院之後，眼睛都不眨一下就直接走出校門。

喂喂，要在校外見面嗎?不是學校的人嗎？那麼對方果然是貴族嗎？

但是夏爾沒有使用公共馬車，像躲避人們目光一樣在小巷裡移動，最後來到了一個看起來治安較差的地區。
感覺像是醉漢摔倒在路上的氣氛，連一隻狗也沒有。

什麼？在這種地方和誰見面？總覺得有既視感，這不是什麼『密會』嗎？在我焦慮變嚴重的時候。

『哈特先生，在您忙的時候打擾了。』

傳來了震動耳朶的聲音。是通訊用的結界。為了不讓夏爾發現，我小聲地回應。

「芙蕾，怎麼了？」

『我現在正在打掃臥室，房間裡都處散亂地放著東西。看起來很重要，請問要怎麼辦？』

啊，是不是把夏爾要的便利道具亂放在那裡了。

「妳隨便把它們靠邊放。」

『明白了。』

說著說著。

「讓妳久等了。」

夏爾在小巷拐角處，站在那裏的是――。

「不，我很高興妳毫不猶豫地來了。很抱歉把妳叫來了。」

銀髮的帥哥……？不會吧，沒想到真的是與年輕的男人會面！？
說起來，我見過這傢伙。

是『１』的人！

他是由學生組成的秘密組織『號碼』的領導人。
他叫什麼名字？我記得是四年級的學生，阿，阿……阿列？

「不，很高興你邀請我，阿列克謝・古貝爾克先生。」

對了對了，就是這個名字的人。無所謂。
沒錯。現在是那種殘念的團體，無所謂。
帥哥在欺騙我妹妹這個事實，就憑這一點――。

「必須消滅……」

『啊！？』

「（纏著夏爾的）壞蟲子，由我親手驅逐……」

『………………』

哎呀，我差點打開了一個奇怪的開關。
我還沒有確定『１』的人是否真的在勾引夏爾。
但是『１』的人啊，如果你敢碰夏爾一根手指頭。 那一刻我就把你拖進神秘時空。

在無盡的黑暗中，給予一點的水和食物使他生存下去。讓糞尿流下來，讓精神被永遠的黑暗侵犯吧！

我半瞪著眼睛，跟在兩人的後面。
沒有任何對話，夏爾一直跟在『１』的人後面走。

「？」

她突然停下來，轉過身來。
我也立刻停止。以腳步踏出的不合理姿勢很辛苦。

「夏洛特君、怎麼了？」

「……」

夏爾一直盯著我看。 什麼？他們發現了嗎？光學迷彩結界應該是完美的……。

「不，沒什麼。」

夏爾回過頭微笑著回應，然後再次跟著『１』的人而行。
沒有被發現……對吧？不過那傢伙，有時會超敏感地察覺到我陷入麻煩的困境。為什麼？

不管怎樣，我又開始繼續跟蹤了。

兩人進入了老舊的集合住宅。感覺好像有點熟悉，但是看來我也該下定決心了。

把小女孩帶進寂靜的建築裡？
是那個嗎？
裡面有他認識的朋友們聚集在一起，會用下流的笑聲來迎接？還有高畫質的相機或許證明什麼。然後對可愛的女孩子，那樣，那樣，那樣，不要——！！

「那麼，妳在這邊的房間換衣服吧。就像事先說的那樣。」

『１』的人帶著扭曲的微笑走過走廊。 夏爾進入指定的房間。
換衣服嗎？一定是穿露出多的衣服之類的吧……等一下。
看見那裡放了幾個木箱。

夏爾打開寫有數字『７』的木箱，裡面就有幾個空瓶子……嗯，是雙重底的吧。裡面有白色的貫頭衣和遮住臉的頭巾。



「剛才已經承蒙介紹了！我是被選為新的『7號』的夏洛特．芬菲斯！」

在最裡面的神秘房間裡，圍著桌子的白頭巾的人們。
在莊重而有趣可笑的氣氛中，額頭上有『７』數字的我的妹妹充滿活力地打了招呼。

「……不，不是說過這裡不用告知名字嗎？」

「哈！？對不起，我不小心。」

夏爾大概在頭巾裡低著頭吧。好可愛。

等一下。
嗯？這是什麼情況？為什麼夏爾會成為搞笑集團的成員？
混亂不堪的我，為了解開這個謎團決定坐在房間的角落――。